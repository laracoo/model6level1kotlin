package com.example.project0608level1

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText

class CustomTextWatcher(edList: Array<EditText>, v: Button) :
        TextWatcher {
        var v: View
        var edList: Array<EditText>
        override fun beforeTextChanged(
            s: CharSequence,
            start: Int,
            count: Int,
            after: Int
        ) {
        }

        override fun onTextChanged(
            s: CharSequence,
            start: Int,
            before: Int,
            count: Int
        ) {
        }

        override fun afterTextChanged(s: Editable) {
                for (editText in edList) {
                    if (editText.text.toString().trim { it <= ' ' }.length <= 0) {
                        v.setEnabled(false)
                        break
                    } else v.setEnabled(true)

                    }
            }

        init {
            this.v = v
            this.edList = edList
        }
    }
// && checkBox.equals(true)
