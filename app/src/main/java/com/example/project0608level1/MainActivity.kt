package com.example.project0608level1

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        login.setEnabled(false)
        check_box.setEnabled(false)


        val edList = arrayOf<EditText>(password, email)
        val textWatcher = CustomTextWatcher(edList, check_box)
        for (editText in edList) editText.addTextChangedListener(textWatcher)

        changeButtonState(check_box)

    }

        fun changeButtonState(checkBox: CheckBox) {
            checkBox.setOnCheckedChangeListener { _, _ ->
                login.isEnabled =
                    !(!checkBox.isChecked || password.length() <= 0 || email.length() <= 0)
            }
        }

        fun onClickLogin() {
            email.isEnabled = false
            password.isEnabled = false
            login.isEnabled = false
            check_box.isEnabled = false

            val view = layoutInflater.inflate(R.layout.progress_activity, container, false)
            container.addView(view)
            val handler = Handler()
            handler.postDelayed(Runnable {
                container.removeView(view)
                results.text = "Login was successful!"
                email.isEnabled = true
                email.text.clear()

                password.setEnabled(true)
                password.getText().clear()

                login.setEnabled(true)

                check_box.setChecked(false)
            }, 2000)
        }
    }

